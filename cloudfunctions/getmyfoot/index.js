// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init()
const db = cloud.database();

// 云函数入口函数
exports.main = async (event, context) => {
  const wxContext = cloud.getWXContext()
  const count = await getCount(wxContext.OPENID)
  let total = count.total
  let list = []
  for (let i = 0; i < total; i += 100) {
    list = list.concat(await getList(wxContext.OPENID, i));
  }

  return {
    event,
    openid: wxContext.OPENID,
    appid: wxContext.APPID,
    unionid: wxContext.UNIONID,
    total: total,
    list: list
  }
}


async function getCount(openid) {
  let count = await db.collection('position').where({
    "_openid": openid
  }).count();
  return count;
}

async function getList(openid,skip) {
  let list = await db.collection('position').where({
    "_openid": openid
  }).orderBy('_id', 'asc').skip(skip).get();
  return list.data;
}