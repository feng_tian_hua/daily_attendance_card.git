// 云函数入口文件
const cloud = require('wx-server-sdk')
cloud.init({
    // API 调用都保持和云函数当前所在环境一致
    env: cloud.DYNAMIC_CURRENT_ENV
})
try {
  exports.main = async (event, context) => {
    //event是index.js传来的data
    cloud.openapi.subscribeMessage.send({
    touser: event._openid,//发送通知给谁的openid
    data: {
      thing1: {  // 
      value: event.thing1
    },
    time3: { // 打卡时间
      value: event.time3
    },
    time6: { // 截止时间
      value: event.time6
    },
    thing8: { // 备注
      value: event.thing8
    }
    },
    templateId: event.templateId,//模板ID
    miniprogramState: 'formal',
    page:'pages/clockIn/clockIn'//这个是发送完服务通知用户点击消息后跳转的页面
    })
  return event
  }
} catch (err) {
  return err
}

