// 云函数入口文件
const cloud = require('wx-server-sdk')

cloud.init({ // API 调用都保持和云函数当前所在环境一致
  env: cloud.DYNAMIC_CURRENT_ENV
})

// 云函数入口函数
exports.main = async (event, context) => {
  const db = cloud.database()
  const _ = db.command
  var repart = {}
  //格式化日期2018-10-02
  const formatDate = date => {
    const year = date.getFullYear()
    const month = date.getMonth() + 1
    const day = date.getDate()
    return [year, month, day].map(formatNumber).join('-')
  }
  //格式化数字
  const formatNumber = n => {
    n = n.toString()
    return n[1] ? n : '0' + n
  }
  var date = formatDate(new Date())

  switch (new Date().getDay()) {
    case 0: repart = {
      'endDay': _.gte(date),
      'startDay': _.lte(date),
      'sunday': 1
    }
      break;
    case 1: repart = {
      'endDay': _.gte(date),
      'startDay': _.lte(date),
      'monday': 1
    }
      break;
    case 2: repart = {
      'endDay': _.gte(date),
      'startDay': _.lte(date),
      'tuesday': 1
    }
      break;
    case 3: repart = {
      'endDay': _.gte(date),
      'startDay': _.lte(date),
      'wednesday': 1
    }
      break;
    case 4: repart = {
      'endDay': _.gte(date),
      'startDay': _.lte(date),
      'thursday': 1
    }
      break;
    case 5: repart = {
      'endDay': _.gte(date),
      'startDay': _.lte(date),
      'friday': 1
    }
      break;
    case 6: repart = {
      'endDay': _.gte(date),
      'startDay': _.lte(date),
      'saturday': 1
    }
      break;
  }

  const promise = await db.collection('clock_project').where(repart).skip(0).orderBy('signTime', 'asc').get()
  const data = promise.data
  for (let i = 0; i < data.length; i++) {
    cloud.openapi.subscribeMessage.send({
      touser: data[i]._openid,//发送通知给谁的openid
      data: {
        thing1: {  // 主题名称
          value: data[i].name
        },
        time3: { // 打卡时间
          value: date + ' ' + data[i].signEarlyTime
        },
        time6: { // 截止时间
          value: date + ' ' + data[i].signTime
        },
        thing8: { // 备注
          value: data[i].remark
        }
      },
      templateId: '7KHoU8piFqzX6_NQpyJP_NC5nu4okVifDhdrEkpzQrI',//模板ID
      miniprogramState: 'formal',
      page: 'pages/clockIn/clockIn'//这个是发送完服务通知用户点击消息后跳转的页面
    })
  }

  return data
}