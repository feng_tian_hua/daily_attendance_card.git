// pages/addCampus/addCampus.js

Page({

  /**
   * 页面的初始数据
   */
  data: {
    creating: false,
    //文本域的内容
    textVal:"",
    //学校名称
    name:'',
    //图片的路径数组
    chooseImgs:[],
    address:'',
    longitude:'',
    latitude:''
  },
  
//选择具体位置
  onChangeAddress: function (e) {
    wx.navigateTo({
      url: "/pages/chooseLocation/chooseLocation"
    });
  },
  //点击“+”选择图片
  handleChooseImg(){
    //调用小程序内置的选择图片api
    wx.chooseImage({
      count: 9,
      sizeType:['original','compressed'],
      sourceType:['album','camera'],
      success: (result) => {
        this.setData({
          //图片数组 进行拼接
          chooseImgs:[...this.data.chooseImgs,...result.tempFilePaths]
        })
        
      }
    })
  },
    //点击自定义图片组件
    handleRemoveImg(e){
      //获取被点击的组件的索引
      const {index} = e.currentTarget.dataset;
      //获取data中的图片数组
      let {chooseImgs}=this.data;
      //删除元素
      chooseImgs.splice(index,1);
      this.setData({
        chooseImgs
      })
    },
    //文本域的输入事件
    bindContextInput(e) {
      this.setData({
        textVal:e.detail.value
      })
    },
    bindKeyInput(e) {
      this.setData({
        name:e.detail.value
      })
    },

  //提交按钮的点击
  bindSubmit(){
    //获取文本域的内容
    const {textVal,chooseImgs,name} = this.data;
    //合法性的验证
    if(!name.trim()){
      wx.showToast({
        title: '请输入学校名称',
        icon:'none',
        //防止用户反复点击
        mask:true,
      })
      return;
    }
    if(!textVal.trim()){
    wx.showToast({
      title: '请输入学校简介',
      icon:'none',
      //防止用户反复点击
      mask:true,
    })
    return;
  }
  var UploadImgs = [];
  wx.showLoading({
    title: '正在上传图片...',
    mask:true
  })
  let promiseArr = [];//创建一个数组来存储一会的promise操作
  //遍历选中的图片，用let定义变量会形成一个闭包，在该作用域执行完之前，变量的值不会发生改变，防止异步导致变量发生变化
  for(let i =0; i< this.data.chooseImgs.length;i++){
    //往数据中push promise操作
    promiseArr.push(new Promise((reslove,reject)=>{
      //一个一个取出图片数组的临时地址
      let item = this.data.chooseImgs[i];
      wx.cloud.uploadFile({
        cloudPath:'images/CampusMap/pictures/' + this.data.name+ i +'.png',//上传至云端的路径
        filePath: item,//小程序临时文件路径
        success: res =>{
          //执行成功的吧云存储的地址一个一个push进去
          UploadImgs.push(res.fileID);
          console.log(res.fileID+"第"+i+"张图片");
          //如果执行成功，就执行成功的回调函数
          reslove();
            wx.hideLoading();
            wx.showToast({
              title: '上传成功！',
            });
        },
        fail:res=>{
          wx.hideLoading();
          wx.showToast({
            title: '上传失败！！！',
          });
        }
        })
    }))
  }
  Promise.all(promiseArr).then(res=>{//等promose数组都做完后做then方法
    console.log("图片上传完成后再执行");
    const db = wx.cloud.database();
          db.collection('school').add({
            data: {
              name:this.data.name,
              type:1,
              detail:this.data.textVal,
              imageUrl:UploadImgs,
              longitude:this.data.longitude,
              latitude:this.data.latitude,
              address:this.data.address
            },
            success: res => {
              // 在返回结果
              wx.showToast({
                title: '添加成功',
                //防止用户反复点击
                mask:true,
              })
              this.setData({
                textVal:"",
                chooseImgs:[],
                name:''
              })
              setTimeout(function () {
                //返回上一个页面
                wx.navigateBack({
                delta: 1,
          })
              }, 800)
            },
            fail: err => {
              wx.showToast({
                icon: 'none',
                title: '添加失败',
              })
            }
          })

  })


  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})