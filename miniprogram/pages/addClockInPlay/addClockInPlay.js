// pages/addClockInPlay/addClockInPlay.js
const app = getApp();
const { $Message } = require('../../dist/base/index');
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		array: [],
		index: 0,
		endIndex: 10,
		indexTime: 6,
    time: ['00:00', '01:00', '02:00', '03:00', '04:00', '05:00', '06:00', '07:00', '08:00', '09:00', '10: 00', '11: 00', '12: 00', '13: 00', '14: 00', '15: 00', '16: 00', '17: 00', '18: 00', '19: 00', '20:00', '21:00', '22:00', '23:00', '24:00'],
		selected: { "monday": false, "tuesday": false, "friday": false, "wednesday": false, "thursday": false, "sunday": false, "saturday": false }
	},
  handleWarning() {
    $Message({
      content: '时间出现错误，开始时间要小于截止时间',
      type: 'warning',
      duration: 8
    });
  },
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
      this.getClockNameList()
	},
	getClockNameList: function() {
		const db = wx.cloud.database();
		const that = this
		db.collection('clock_name').get(
			{
				success: res => {
					let arr = []
					// res.data 是包含以上定义的两条记录的数组
					for (let i=0;i<res.data.length;i++) {
            arr.push(res.data[i].name)
					}
					that.setData({
						array: arr
					})
				}
			}
		)
	},
	bindPickerChange: function (e) {
		console.log('picker发送选择改变，携带值为', e.detail.value)
		this.setData({
			index: e.detail.value
		})
	},
	bindTimeChange: function (e) {
		var endIndex = this.data.endIndex;
		var starIndex = this.data.indexTime;
		var time = parseInt(e.detail.value)
		if (e.currentTarget.id == "startTime") {
				this.setData({
					indexTime: time
			})
      starIndex = time;
		} else if (e.currentTarget.id == "endTime") {
				this.setData({ endIndex: time });
        endIndex = time;
		}
    if (endIndex <= starIndex){
      this.handleWarning();
    }
	},
	// checkboxChange: function (e) {
	// 	var selectedList = e.detail.value;
	// 	var date = ["monday", "tuesday", "friday", "wednesday", "thursday", "sunday", "saturday"];//包含所有日期的数组
	// 	var selected = this.data.selected;//先获取data中的值，好用来赋值
	// 	for (var i = 0; i < date.length; i++) {
	// 		if (selectedList.indexOf(date[i]) != -1) { //判断所有的日期date在所选择的列表中是否存在，存在则给class
	// 			selected[date[i]] = true;
	// 		} else {
	// 			selected[date[i]] = false;
	// 		}
	// 	}
	// 	this.setData({ selected: selected });
	// },
	formSubmit: function (e) {
		//提交后台
		var startTime = parseInt(e.detail.value.startTime);
		var endTime = parseInt(e.detail.value.endTime);
		var playName = e.detail.value.playName;
		var detail = e.detail.value.detail;
		// console.log(startTime+playName+endTime+detail);
    if(startTime>=0 && endTime && playName &&detail){
			const db = wx.cloud.database();
			db.collection('clock_project').add({
				data:{
				finish_time : endTime,
				star_time : startTime,
				name: playName,
				detail: detail,
				main: 0,
				num: 0
			},
				success: res => {
					// 在返回结果
					wx.showToast({
						title: '添加成功',
					})
					setTimeout(function(){
						wx.navigateBack({

						})
					},800)
					// console.log('[数据库] [新增记录] 成功，记录: ', res)
				},
					fail: err => {
						wx.showToast({
							icon: 'none',
							title: '添加失败,该名称你已使用过了'
						})
						console.error('[数据库] [新增记录] 失败：', err)
					}
		})
		}else{
			wx.showToast({
				icon: 'none',
				title: '请填写完整信息后再提交',
			})
		}
	},
	//取消
	cancel: function(){
		wx.navigateBack({
			
		})
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})