const app = getApp();
var util = require('../../utils/util.js');
var _openid ='';
Page({
  data: {
    formats: {},
    readOnly: false,
    placeholder: '开始输入...',
    editorHeight: 300,
    keyboardHeight: 0,
    addTime: '',
    time: '',
    title: '',
    inputValue: '',
    qlHeight: 0,
    note: '',
    isIOS: false,
    editorCtx: Object
  },
  readOnlyChange() {
    this.setData({
      readOnly: !this.data.readOnly
    })
  },
  onLoad() {
    setTimeout(
			function(){
		  _openid = app.getOpenid();
		},1000)
    const platform = wx.getSystemInfoSync().platform
    const isIOS = platform === 'ios'
    this.setData({ isIOS})
    const that = this
    this.updatePosition(0)
    let keyboardHeight = 0
    wx.onKeyboardHeightChange(res => {
      if (res.height === keyboardHeight) return
      const duration = res.height > 0 ? res.duration * 1000 : 0
      keyboardHeight = res.height
      setTimeout(() => {
        wx.pageScrollTo({
          scrollTop: 0,
          success() {
            that.updatePosition(keyboardHeight)
            that.editorCtx.scrollIntoView()
          }
        })
      }, duration)

    })
  },
  // 获取标题
  bindKeyInput(e) {
    console.log(e)
    this.setData({
      inputValue: e.detail.value
    })
  },
  updatePosition(keyboardHeight) {
    var that = this
    var query = wx.createSelectorQuery();
    query.select('.title').boundingClientRect();
    query.select('.time').boundingClientRect();
    query.selectViewport().scrollOffset();
    query.exec((res) => {
      var titleHeight = res[0].height; // 获取list高度
      var timeHeight = res[1].height; // 获取list高度
      const toolbarHeight = 50
      const { windowHeight, platform } = wx.getSystemInfoSync()
      let editorHeight = keyboardHeight > 0 ? (windowHeight - keyboardHeight - toolbarHeight - titleHeight - timeHeight) : windowHeight
      that.setData({ editorHeight, keyboardHeight })
    });

  },
  calNavigationBarAndStatusBar() {
    const systemInfo = wx.getSystemInfoSync()
    const { statusBarHeight, platform } = systemInfo
    const isIOS = platform === 'ios'
    const navigationBarHeight = isIOS ? 44 : 48
    return statusBarHeight + navigationBarHeight
  },
  onEditorReady: async function() {
    const that = this
    wx.createSelectorQuery().select('#editor').context(function (res) {
      that.editorCtx = res.context
       setTimeout(function(){
        console.log(that.editorCtx)
        that.editorCtx.setContents({
          delta: '',
          success: res => {
            console.log(res)
          },
          fail: error =>{
            console.log(error)
          }
        })
      },1000)
    }).exec()

  },
  blur() {
    this.editorCtx.blur()
  },
  format(e) {
    let { name, value } = e.target.dataset
    if (!name) return
    // console.log('format', name, value)
    this.editorCtx.format(name, value)

  },
  onStatusChange(e) {
    const formats = e.detail
    this.setData({ formats })
  },
  insertDivider() {
    this.editorCtx.insertDivider({
      success: function () {
        console.log('insert divider success')
      }
    })
  },
  clear() {
    this.editorCtx.clear({
      success: function (res) {
        console.log("clear success")
      }
    })
  },
  removeFormat() {
    this.editorCtx.removeFormat()
  },
  insertDate() {
    const date = new Date()
    const formatDate = `${date.getFullYear()}/${date.getMonth() + 1}/${date.getDate()}`
    this.editorCtx.insertText({
      text: formatDate
    })
  },
 //获取编辑器输入的内容
  getContent() {
    const that = this
    if(that.data.inputValue){
    that.editorCtx.getContents({
      success: res => {
        const db = wx.cloud.database()
        db.collection('memorandum').add({
          data: {
             doc: res.delta,
             noteTitle: that.data.inputValue,
             addTime: this.data.addTime
            }
        })
        wx.navigateTo({
          url: '../memorandum/memorandum',
        })
      },
      fail: error =>{
        console.log(error)
      },

    })
  } else{
    wx.showToast({
      title: '请填写标题！',
      icon: 'error',
    })
  }
  },
  // 插入图片
  insertImage() {
    const that = this
    wx.chooseImage({
      count: 1,
      success: function (res) {
        wx.cloud.uploadFile({
          cloudPath:'memorandum/uploadimages/'+ _openid + '/' + Math.floor(Math.random()*1000000) + res.tempFilePaths[0].match(/\.[^.]+?$/)[0],
          filePath: res.tempFilePaths[0],
          success(res){
            console.log("成功",res);
            that.editorCtx.insertImage({
              src: res.fileID,
              data: {
                id: 'abcd',
                role: 'god'
              },
              width: '80%',
              success: function () {
                console.log('insert image success')
              }
            })
          }
        })

      }
    })
  },
  inittime: function() {
    this.setData({
      addTime: new Date(),
      time: util.getHM(new Date())
    })
  },
   /**
	 * 生命周期函数--监听页面显示
	 */
	onShow() { //返回显示页面状态函数
		//错误处理
		this.inittime();//再次加载，实现返回上一页页面刷新
		//正确方法
		//只执行获取地址的方法，来进行局部刷新
  },
  	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady(e) {
	  var that = this
    // 页面渲染完成
    // const that = this
    // that.editorCtx.insertText()

	},
})
