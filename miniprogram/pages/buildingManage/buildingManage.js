// pages/buildingManage/buildingManage.js
var redis;
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		id: '',
		building: '',
		current: 1,
		pageSize: 0,
		load: true
	},
	slideButtonTap(e) {
		if (e.detail.index == 0) {
			this.edit(e.detail.data._id)
		} else {
			this.delete(e.detail.data._id);
		}
	},
	edit: function (e) {
		var _id = e;
		const db = wx.cloud.database();
		var info = {}
		db.collection('school').doc(_id).get({
			success: function (res) {
				// res.data 包含该记录的数据
				info = res.data;
			}
		})
		setTimeout(function () {
			wx.navigateTo({
				url: '../editBuilding/editBuilding?id=' + _id,
				success: function (res) {
					// 通过eventChannel向被打开页面传送数据
					res.eventChannel.emit('acceptDataFromOpenerPage', { data: info })
				}
			})
		}, 800)
	},
	delete: function (e) {
		var _id = e;
		var that = this;
		wx.showModal({
			title: '提示',
			content: '确认删除该地点吗？',
			success(res) {
				if (res.confirm) {
					console.log('用户点击确定');
					if (_id) {
						const db = wx.cloud.database()
						db.collection('school').doc(_id).remove({
							success: res => {

								wx.showToast({
									title: '删除成功',
								})
								that.onShow()
							},
							fail: err => {
								wx.showToast({
									icon: 'none',
									title: '删除失败',
								})
								console.error('[数据库] [删除记录] 失败：', err)
							}
						})
					} else {
						wx.showToast({
							title: '无记录可删，请见创建一个记录',
						})
					}
				} else if (res.cancel) {
					console.log('用户点击取消')
				}
			}
		})
	},
	addBuilding: function () {
		wx.navigateTo({    //保留当前页面，跳转到应用内的某个页面（最多打开5个页面，之后按钮就没有响应的）
			url: "../addBuilding/addBuilding?id=" + this.data.id,
		})
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		redis = options;
		this.setData({
			current: 1,
			load: true,
			id: options.id
		})
		this.queryData(0, options.id)
	},
	queryData(pageNum, id) {
		var that = this;
		wx.cloud.callFunction({
			name: 'show',
			data: {
				type: 2,
				pageNum: pageNum,
				foreign: id
			},
			success: res => {
				let total = res.result.total
				console.log(total);
				that.setData({
					load: false,
					building: res.result.data
				})
				if (total % 10 > 0) {
					that.setData({
						pageSize: parseInt(total / 10) == 0 ? 1 : parseInt(total / 10) + 1
					})
				} else {
					that.setData({
						pageSize: parseInt(total / 10) == 0 ? 1 : parseInt(total / 10)
					})
				}
			}
		})
	},
	// 分页显示
	handleChange(e) {
		if (e != 1) {
			const type = e.currentTarget.id;
			if (type === 'next') {
				this.setData({
					current: this.data.current + 1
				});
			} else if (type === 'prev') {
				this.setData({
					current: this.data.current - 1
				});
			}
		}
		this.queryData(this.data.current - 1);
	},

	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {
		this.handleChange(1)
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})