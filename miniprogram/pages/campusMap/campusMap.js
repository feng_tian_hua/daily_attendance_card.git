
Page({
  data: {
    caiItems: [{}],
    loading: true,
    hasMore: false,
    page: 1,
    height: "",
    name:'',
    inpValue:'',
    //副本，存放页面最初加载的对象数组
    fuben:[{}]
  },
  TimeId:-1,
  //点击取消按钮时
  handleCancel(){
    this.setData({
      inpValue:"",
      caiItems:this.data.fuben
    })
  },
  //输入框的值改变，就会触发的事件
  handleInput(e){
      //获取输入框的值
      const {value}=e.detail;
      //监测合法性
      if(!value.trim()){
        this.setData({
          caiItems:this.data.fuben
        })
      }
      //准备发送请求获取数据
      clearTimeout(this.TimeId);
      this.TimeId=setTimeout(() => {
        this.qsearch(value);
      },700)
  },
  //发送请求获取搜索建议 数据
   qsearch(query){
     var that = this
    const db = wx.cloud.database();
    const _ = db.command
    db.collection('school').where(_.or([
      
      //使用正则查询，实现对搜索的模糊查询
      {name:db.RegExp({
        //从搜索栏中获取的value作为规则进行匹配
        regexp: query,
        //大小写不区分
        options:'i'
      })
      },
      {
        'detail':db.RegExp({
          //从搜索栏中获取的value作为规则进行匹配
          regexp: query,
          //大小写不区分
          options:'i'
      })
    }
    ]).and([{
      type:1,
  }])).get({
      success:function(res) {
        if(res.data.length>0){
          var info = [{}];
          for(var i = 0; i< res.data.length; i++){
            info[i] = {name: res.data[i].name, detail: res.data[i].detail, imageUrl: res.data[i].imageUrl, detailHrefId: res.data[i]._id};
          }
          that.setData({
            caiItems: info
          })
        }else {
          that.setData({
            caiItems:''
          })
        }
      },
    })
  },
  // 页面初始化 options为页面跳转所带来的参数
  onLoad: function (options) {
    var that = this;
    const db = wx.cloud.database()
    db.collection('school').where({
      type : 1
    }).get({
      success: function(res) {
        if(res.data.length>0){
          var info = [{}];
          for(var i = 0; i< res.data.length; i++){
            info[i] = {name: res.data[i].name, detail: res.data[i].detail, imageUrl: res.data[i].imageUrl, detailHrefId: res.data[i]._id, 
              latitude: res.data[i].latitude, longitude: res.data[i].longitude};
          }
          that.setData({
            caiItems: info,
            fuben:info
          })
          
        }
      }
    })
    
    this.getDataFromServer(that, this.data.page)
  },
  onReady: function () {
    var that = this
    // 页面渲染完成
    var query = wx.createSelectorQuery();
    query.select('.searchbar').boundingClientRect();
    query.select('.mainFrame').boundingClientRect();
    query.selectViewport().scrollOffset();
    query.exec((res) => {
      var listHeight = res[1].height - res[0].height; // 获取list高度
      console.log(res)
      that.setData({
        height: "height:" + listHeight * 2 + "rpx"
      })
    });
  },
  onShow: function () {
    // 页面显示
  },
  onHide: function () {
    // 页面隐藏
  },
  onUnload: function () {
    // 页面关闭
  },
  handleChange: function (e) {
    let current = e.detail.current
    let volsLength = this.data.caiItems.length
    // if(current==1){
    //     this.setData({
    //         page: 1,
    //     caiItems:this.data.caiItems.slice(0,15)
    //     })

    //     console.log("上拉拉加载更多...." + this.data.page)

    //     this.getDataFromServer(that,this.data.page)
    // }
    if (current > (volsLength - 5)) {
      this.setData({ page: this.data.page + 1 })

      console.log("上拉拉加载更多...." + this.data.page)

      this.getDataFromServer(that, this.data.page)
    }

  },


  refresh: function () {
    console.log("下拉刷新....")
    this.onLoad()
  },
  loadMore: function () {

    this.setData({ page: this.data.page + 1 })

    console.log("上拉拉加载更多...." + this.data.page)

    this.getDataFromServer(that, this.data.page)
  },
  //获取网络数据的方法
  getDataFromServer: function (that, page) {
    that.setData({
      loading: false,
      hasMore: true
    })
    //调用网络请求
    // wx.request({
    //     //连到服务器 获取json格式的文档
    //     url: Constant.SERVER_ADDRESS+"/focuNews"+"?pageId="+page,
    //     header: {
    //     },
    //     success: function (res) {
    //         if (res == null) {  
    //             console.error(Constant.ERROR_DATA_IS_NULL);
    //             return;
    //         }
    //         if(res.data.htmlBody.length>0){
    //             that.setData({
    //                 caiItems:that.data.caiItems.concat(res.data.htmlBody) , loading: true, hasMore: false
    //         })
    //         }

    //     }
    // });
  },


  //点击 跳转到具体页面
  onItemClick: function (event) {
    
    var targetUrl = "/pages/campusMapBehind/campusMapBehind"
    
    var queryBean = JSON.stringify(event.currentTarget.dataset.msg)

    if (event.currentTarget.dataset.detailHrefId != null)
    
      targetUrl = targetUrl + "?Id=" + event.currentTarget.dataset.detailHrefId + "&bean=" + queryBean ;
     
    wx.navigateTo({
      url: targetUrl
    });
  },
})

var that;
//获取配置的的 全局常量