

Page({

  /**
   * 页面的初始数据
   */
  data: {
      item:[],
      id:8,
      lat:'',
      lng:''
  },
  click(e) {
    wx.openLocation({
      latitude: parseFloat(this.data.lat),
      longitude: parseFloat(this.data.lng),
      name: e.currentTarget.dataset.name,
      success: function() {
        console.log('success');
      },
      fail: res => {
        // console.log(res);
      }
    });
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    const db = wx.cloud.database()
    
        db.collection('school').where({
          _id:options.Id
        }).get({
          success: function(res) {
            var info = {}
            if(res.data.length>0){
            
              info=  {name: res.data[0].name, detail: res.data[0].detail, imageUrl: res.data[0].imageUrl, detailHrefId: res.data[0]._id};
            
            }
              that.setData({
                item: info,
                lat: res.data[0].latitude, 
                lng: res.data[0].longitude
              })
            }
          })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})