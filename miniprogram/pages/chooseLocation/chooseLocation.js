var QQMapWX = require('../../utils/qqmap-wx-jssdk.js');
var qqmapsdk;
Page({
  data: {
    latitude: 0,//地图初次加载时的纬度坐标
    longitude: 0, //地图初次加载时的经度坐标
    name:"", //选择的位置名称
  },
  onLoad: function (options) {
    // 实例化API核心类
    qqmapsdk = new QQMapWX({
      key: 'YE4BZ-QGJEJ-5GYFJ-KYPN5-CDRBS-ZGBPZ'
    }); 
    this.moveToLocation();   
  },
  //移动选点
  moveToLocation: function () {
    wx.chooseLocation({
      success: function (res) {    
        let pages = getCurrentPages();//当前页面
        let prevPage = pages[pages.length-2];//上一页面
        prevPage.setData({//直接给上移页面赋值
        address:res.address,
        longitude:res.longitude,
        latitude: res.latitude,
        });
        console.log(res);    
        //选择地点之后返回到原来页面
        wx.navigateBack({
          delta: 1,
        });
      },
      fail: function (err) {
        wx.navigateBack({
          delta: 1,
        });
      }
    });
  }
});