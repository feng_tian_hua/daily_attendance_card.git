const app = getApp();
var util = require('../../utils/util.js');
var _openid = '';
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		step: 1,
		isClock: false, // 该计划今日是否已打卡
		openid: '',
		count: null,
		queryResult: '1',
		nickName: '',
		flag: 0,
		clockInNames: [{}],
		index: 0,
		selectText: '',
		urlimage: '',
		starTime: '',
		finishTime: '',
		num: 0,
		projectId: '',
		showI: false
	},
	onPullDownRefresh: function () {
		wx.setStorage({ key: "send", data: false })
		wx.showNavigationBarLoading() //在标题栏中显示加载
		this.userClickInfo();
		setTimeout(function () {

			wx.hideNavigationBarLoading() //完成停止加载

			wx.stopPullDownRefresh() //停止下拉刷新

		}, 1500);
	},
	onLoad: function () {

		// wx.showLoading({
		// 	title: '加载中',
		// 	mask: true
		// })

		setTimeout(
			function () {
				_openid = app.getOpenid();
			}
			, 1000)
		wx.startPullDownRefresh()

	},
	userClickInfo: function () {
		var that = this;
		var openid = _openid;
		const db = wx.cloud.database();
		var frist = 1;
		const _ = db.command
		var repart = {}
		var date = util.formatDate(new Date())
		// 查询参数
		switch (util.getW(new Date)) {
			case 0: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'sunday': 1
			}
				break;
			case 1: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'monday': 1
			}
				break;
			case 2: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'tuesday': 1
			}
				break;
			case 3: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'wednesday': 1
			}
				break;
			case 4: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'thursday': 1
			}
				break;
			case 5: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'friday': _.eq(1)
			}
				break;
			case 6: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'saturday': 1
			}
				break;
		}
		//查询用户主页的打卡项目列表
		db.collection('clock_project').where(repart).skip(0).orderBy('main', 'desc').get({
			success: res => {
				if (res.data.length != 0) {
					this.setData({
						clockInNames: res.data
					});
					that.listInfo();

				} else {
					//新用户初始化
					// db.collection('clock_project').add({
					// 	data: {
					// 		name: '测试计划', // 打卡名称
					// 		signEarlyTime: '00:00', // 开始时间
					// 		signTime: '00:00', // 截止时间
					// 		startDay: '2021-01-00', // 开始日期
					// 		endDay: '2025-12-00', // 截止日期
					// 		'monday': 1,
					// 		'tuesday': 1,
					// 		'wednesday': 1,
					// 		'thursday': 1,
					// 		'friday': 1,
					// 		'saturday': 0,
					// 		'sunday': 0,
					// 		remark: '测试备注',// 备注
					// 		imageId: '837692d3-28fc-4b8d-a4e2-2fb5ba7b0d0a', // 图片信息ID
					// 		num: 0
					// 	},
					// 	success: res => {
					// 		that.setData({
					// 			clockInNames: [{
					// 				name: '测试计划', // 打卡名称
					// 				signEarlyTime: '00:00', // 开始时间
					// 				signTime: '00:00', // 截止时间
					// 				startDay: '2021-01-00', // 开始日期
					// 				endDay: '2025-12-00', // 截止日期
					// 				'monday': 1,
					// 				'tuesday': 1,
					// 				'wednesday': 1,
					// 				'thursday': 1,
					// 				'friday': 1,
					// 				'saturday': 0,
					// 				'sunday': 0,
					// 				remark: '测试备注',// 备注
					// 				imageId: '837692d3-28fc-4b8d-a4e2-2fb5ba7b0d0a', // 图片信息ID
					// 				num: 0
					// 			}]
					// 		})
					// 		that.listInfo();
					// 	},
					// 	fail: err => {
					// 		console.error('[数据库] [新增记录] 失败：', err)
					// 	}
					// })
					that.setData({
						clockInNames: [{
							name: '暂无打卡计划',
							imageId: '837692d3-28fc-4b8d-a4e2-2fb5ba7b0d0a', // 图片信息ID
						}]
					})
					that.listInfo();
				}

			}
		});

	},
	listInfo() {
		const db = wx.cloud.database();
		var time = util.formatTime(new Date());
		var index = this.data.index;
		var imageId = this.data.clockInNames[index].imageId
		var name = this.data.clockInNames[index].name;
		// 查询今日打卡记录
		db.collection('clock_in').where({
			_openid: this.data.openid,
			date: time.slice(0, 10),
			name: name
		}).skip(0).get({
			success: res => {
				var flag = false
				if (res.data.length > 0)
					flag = true
				this.setData({
					isClock: flag
				});
				this.setData({
					starTime: this.data.clockInNames[this.data.index].signEarlyTime,
					finishTime: this.data.clockInNames[this.data.index].signTime,
					num: this.data.clockInNames[this.data.index].num,
					projectId: this.data.clockInNames[this.data.index]._id,
					selectText: this.data.clockInNames[this.data.index].remark
				})
				this.onTimeInfo();
			},
			fail: err => {
				wx.showToast({
					icon: 'none',
					title: '查询失败！！'
				});
				console.error('[数据库] [查询记录] 失败：', err)
			}
		})
		// 查询句子和图片
		db.collection('clock_in_text').where({
			_id: imageId
		}).skip(0).get({
			success: res => {
				if (res.data.length != 0) {
					this.setData({
						urlimage: res.data[0].image
					});
				}
			}
		});

	},
	onTimeInfo: function () {
		var time = util.formatTime(new Date());
		var timeOf = util.getTimestamp(time)
		var that = this;
		var isClock = that.data.isClock;
		var finishTime = util.getTimestamp(time.slice(0, 11) + that.data.finishTime);
		var starTime = util.getTimestamp(time.slice(0, 11) + that.data.starTime);
		console.log(time.slice(0, 11) + that.data.finishTime + "--" + time)
		if (isClock) {
			wx.showToast({
				icon: 'none',
				title: '今日该计划你已打卡！！',
				duration: 2000
			});
		} else if (timeOf >= finishTime) {
			wx.showToast({
				icon: 'none',
				title: '今日你已错过该计划的打卡时间！！请按时打卡呦',
				duration: 2000
			});
		} else if (timeOf < starTime) {
			wx.showToast({
				icon: 'none',
				title: '今日还没到该计划的打卡时间！请耐心等待。。',
				duration: 2000
			});
		} else if (!isClock && timeOf < finishTime) {
			wx.showToast({
				icon: 'none',
				title: '今日你还没打卡呦！！',
				duration: 2000
			});
		}
	},

	//打卡
	onAdd: function (e) {

		const db = wx.cloud.database();
		//系统时间
		var time = util.formatTime(new Date());
		var timeOf = util.getTimestamp(time)
		var that = this;
		var isClock = that.data.isClock;
		var finishTime = util.getTimestamp(time.slice(0, 11) + that.data.finishTime);
		var starTime = util.getTimestamp(time.slice(0, 11) + that.data.starTime);
		//打卡项目名称
		var _index = e.detail.value.clockInName;
		var name = this.data.clockInNames[_index].name

		if (!isClock && timeOf < finishTime && timeOf >= starTime) {
			db.collection('clock_in').add({
				data: {
					time: new Date(time),
					date: time.slice(0, 10),
					name: name
				},
				success: res => {
					// 在返回结果中会包含新创建的记录的 _id
					this.setData({
						isClock: true,
						count: 1
					})
					wx.showToast({
						title: '打卡成功',
						duration: 2000
					})
					this.clockInNum();
				},
				fail: err => {
					wx.showToast({
						icon: 'none',
						title: '打卡失败',
						duration: 2000
					})
					console.error('[数据库] [新增记录] 失败：', err)
				}
			})
		} else if (isClock) {
			wx.showToast({
				icon: 'none',
				title: '打卡失败，今日已打卡！！',
				duration: 2000
			});
		} else if (timeOf >= finishTime) {
			wx.showToast({
				icon: 'none',
				title: '打卡失败，今日你已错过该计划打卡时间！！',
				duration: 2000
			});
		} else if (timeOf > finishTime) {
			wx.showToast({
				icon: 'none',
				title: '打卡失败，没到打卡时间！！',
				duration: 2000
			});
		}
	},
	//更新打卡次数
	clockInNum: function () {
		var num = this.data.num + 1;
		const db = wx.cloud.database();
		db.collection('clock_project').doc(this.data.projectId).update
			({
				data: {
					num: num
				},
				success: res => {
					console.log("统计", num)
				},
				fail: err => {
					icon: 'none',
						console.error('[数据库] [更新记录] 失败：', err)
				}
			})
	},
	bindGetUserInfo(e) {
		this.setData({

		})
	},

	// clockProject: function(){
	// 	wx.showToast({
	// 		icon: 'none',
	// 		title: '该功能正在开发中，尽请期待！！',
	// 	})
	// },
	bindPickerChange: function (e) {
		this.setData({
			index: e.detail.value
		})
		console.log("index:", e.detail.value)
		this.listInfo();
	},
	onShow: function () {

	},
	//允许接收服务通知
	requestSubscribeMessage() {
		wx.getStorageSync('send')
		var that = this
		const templateId = '7KHoU8piFqzX6_NQpyJP_NC5nu4okVifDhdrEkpzQrI'//填入你自己想要的模板ID，记得复制粘贴全，我自己因为网页没开全，结果浪费半小时
		// const templateId2='9iF2Sid0tiFEsD3SWefVzcsNSuSUPqrBFYBDo1zGnEo'//可以多个模板ID，然后加入下面的tmplIds，现在一个小程序最多三个
		// const templateId3='B7taYlgSSRTEZuOLH5pMIDr4feVLYt7W1DiqcPtZyDQ'
		if (true) {
			wx.requestSubscribeMessage({
				//tmplIds: [templateId,templateId2,templateId3],
				tmplIds: [templateId],
				success: (res) => {
					//if (res[templateId] === 'accept'&&res[templateId2] === 'accept'&&res[templateId3] === 'accept') {
					if (res[templateId] === 'accept') {

						wx.setStorage({ key: "send", data: true })

					} else {
						wx.setStorage({ key: "send", data: true })
					}
				},
				fail: (err) => {
					console.log(err)
				},
			})
		}
		this.setInterval()
	},
	// 定时查询发送订阅消息
	setInterval() {
		const db = wx.cloud.database()
		const _ = db.command
		var repart = {}
		var date = util.formatDate(new Date())

		switch (util.getW(new Date)) {
			case 0: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'sunday': 1
			}
				break;
			case 1: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'monday': 1
			}
				break;
			case 2: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'tuesday': 1
			}
				break;
			case 3: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'wednesday': 1
			}
				break;
			case 4: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'thursday': 1
			}
				break;
			case 5: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'friday': _.eq(1)
			}
				break;
			case 6: repart = {
				_openid: _openid,
				'endDay': _.gte(date),
				'startDay': _.lte(date),
				'saturday': 1
			}
				break;
		}
		console.log(repart)
		db.collection('clock_project').where(repart).skip(0).orderBy('main', 'desc').get({
			success: res => {
				console.log(res)
				for (let i = 0; i < res.data.length; i++) {
					// 发送消息
					wx.cloud.callFunction({
						name: 'sende',
						//data是用来传给云函数event的数据，你可以把你当前页面获取消息填写到服务通知里面
						data: {
							action: 'sendSubscribeMessage',
							templateId: '7KHoU8piFqzX6_NQpyJP_NC5nu4okVifDhdrEkpzQrI',//这里我就直接把模板ID传给云函数了
							thing1: res.data[i].name,
							time3: date + ' ' + res.data[i].signEarlyTime,
							time6: date + ' ' + res.data[i].signTime,
							thing8: res.data[i].remark,
							_openid: res.data[i]._openid
						},
						success: res => {
							console.warn('[云函数] [openapi] subscribeMessage.send 调用成功：', res)

						},
						fail: err => {
							wx.showToast({
								icon: 'none',
								title: '调用失败',
							})
							console.error('[云函数] [openapi] subscribeMessage.send 调用失败：', err)
						}
					})
				}
			}
		})

	},
	// //发送消息
	// sendSubscribeMessage(e) {
	// 	//调用云函数，
	// 	wx.cloud.callFunction({
	// 		name: 'sende',
	// 		//data是用来传给云函数event的数据，你可以把你当前页面获取消息填写到服务通知里面
	// 		data: {
	// 			action: 'sendSubscribeMessage',
	// 			templateId: '7KHoU8piFqzX6_NQpyJP_NC5nu4okVifDhdrEkpzQrI',//这里我就直接把模板ID传给云函数了
	// 			thing1: '打卡提醒',
	// 			time3: '2019/12/9 14:00',
	// 			time6: '2019/12/9 14:00',
	// 			thing8: 'Test_activity',
	// 			_openid: _openid
	// 		},
	// 		success: res => {
	// 			console.warn('[云函数] [openapi] subscribeMessage.send 调用成功：', res)

	// 		},
	// 		fail: err => {
	// 			wx.showToast({
	// 				icon: 'none',
	// 				title: '调用失败',
	// 			})
	// 			console.error('[云函数] [openapi] subscribeMessage.send 调用失败：', err)
	// 		}
	// 	})
	// },


})


