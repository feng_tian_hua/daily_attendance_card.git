// pages/editCampus/editCampus.js

Page({

  /**
   * 页面的初始数据
   */
  data: {
    //按钮变换
    creating: false,
    //文本域的内容
    textVal:"",
    //前一页提交的信息
    info:{},
    //学校名称
    name:'',
    //选择的图片
    chooseImgs:[],
    fuben:[],
    address:'',
    longitude:'',
    latitude:''
  },

  onChangeAddress: function (e) {
    wx.navigateTo({
      url: "/pages/chooseLocation/chooseLocation"
    });
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    var that = this
    const eventChannel = this.getOpenerEventChannel()
		// 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
		eventChannel.on('acceptDataFromOpenerPage', function (msg) {
			that.setData({
        //副本，存储原来的图片数组与新增的作对比
        fuben:msg.data.imageUrl,
        info: msg.data,
        chooseImgs:msg.data.imageUrl,
        name:msg.data.name,
        textVal:msg.data.detail,
        address:msg.data.address,
        latitude:msg.data.latitude,
        longitude:msg.data.longitude
		 })
		})
  },
//点击“+”选择图片
handleChooseImg(){
  //调用小程序内置的选择图片api
  wx.chooseImage({
    count: 9,
    sizeType:['original','compressed'],
    sourceType:['album','camera'],
    success: (result) => {
      this.setData({
        //图片数组 进行拼接
        chooseImgs:[...this.data.chooseImgs,...result.tempFilePaths]
      })
      
    }
  })
},
  //点击自定义图片组件
  handleRemoveImg(e){
    //获取被点击的组件的索引
    const {index} = e.currentTarget.dataset;
    //获取data中的图片数组
    let {chooseImgs}=this.data;
    //删除元素
    chooseImgs.splice(index,1);
    this.setData({
      chooseImgs
    })
  },
  //文本域的输入事件
  bindContextInput(e) {
    this.setData({
      textVal:e.detail.value
    })
  },
  bindKeyInput(e) {
    this.setData({
      name:e.detail.value
    })
  },
  //上传图片与原有图片比对
   compare(v){
    let flag = false;
    this.data.fuben.forEach((w,i)=>{
        if(w === v){
          flag = true;
        }
    })
    return flag;
  },
      //上传图片部分
       upload(){
        let UploadImgs = [];
      wx.showLoading({
        title: '正在上传图片...',
        mask:true
      })
      let promiseArr = [];
      this.data.chooseImgs.forEach((v,i)=>{
        promiseArr.push(new Promise((resolve,reject)=>{
          let j = this.compare(v);
        if(j){
          UploadImgs.push(v);
          // if(i == this.data.chooseImgs.length-1){
          //   wx.hideLoading();
          //   this.update(UploadImgs);
          // }
          resolve();
        }else{
        wx.cloud.uploadFile({
        cloudPath:'images/CampusMap/pictures/' + this.data.name + new Date().getTime() +'.png',//上传至云端的路径
        filePath: v,//小程序临时文件路径
        success: res =>{
          UploadImgs.push(res.fileID);
          resolve();
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '上传失败',
          })
        }
    })
  }
        }))
       //所有图片都上传完毕了才触发
       Promise.all(promiseArr).then(res=>{
         if(i == this.data.chooseImgs.length-1){
        wx.hideLoading();
        this.update(UploadImgs);
      }
    })
    })
    
  },
    //数据库更新
    update(UploadImgs){ 
      wx.showToast({
      title: '修改中...',
      icon: 'loading',
      duration: 5000
    });
    const db = wx.cloud.database();
      db.collection('school').doc(this.data.info._id).update({
        data: {
          name:this.data.name,
          detail:this.data.textVal,
          imageUrl:UploadImgs,
          longitude:this.data.longitude,
          latitude:this.data.latitude,
          address:this.data.address
        },
        success: res => {
          // 在返回结果
          wx.showToast({
            title: '修改成功',
            //防止用户反复点击
            mask:true,
          })
          this.setData({
            textVal:"",
            chooseImgs:[],
            name:''
          })
          setTimeout(function () {
            //返回上一个页面
            wx.navigateBack({
            delta: 1,
      })
          }, 800)
        },
        fail: err => {
          wx.showToast({
            icon: 'none',
            title: '修改失败'
          })
          console.error('[数据库] [更新记录] 失败：', err)
        }
      })},
    //修改按钮的点击
    bindSubmit(){
      //获取文本域的内容
      const {textVal,name} = this.data;
      //合法性的验证
      if(!name.trim()){
        wx.showToast({
          title: '请输入学校名称',
          icon:'none',
          //防止用户反复点击
          mask:true,
        })
        return;
      }
      if(!textVal.trim()){
      wx.showToast({
        title: '请输入学校简介',
        icon:'none',
        //防止用户反复点击
        mask:true,
      })
      return;
    };
    if(this.data.chooseImgs.length == 0){
      wx.showToast({
        title: '请至少上传一张图片',
        icon:'none',
        //防止用户反复点击
        mask:true,
      })
      return;
    };
    this.upload();
    
    },
  
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})