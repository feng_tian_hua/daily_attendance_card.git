var util = require('../../utils/util.js');
var app = getApp();

Page({
  data: {
    task: {},
    toDay: util.formatDate(new Date()),
    creating: false,
    button: {
      txt: '修改'
    },
    modalHidden: true,
    dialogShow: false,
    buttons: [{ text: '取消' }, { text: '确定' }],
    imageName: '',
    imageList: [], // 图片列表
  },
  // 提示
  handleWarning() {
    wx.showModal({
      title: '温馨提示',
      content: '开始时间不能大于截止时间',
      showCancel: false
    })
  },
  // 设置任务名称
  bindKeyInput: function (e) {
    this.setData({
      'task.name': e.detail.value
    });
  },
  // 设置备注
  bindContextInput: function (e) {
    this.setData({
      'task.remark': e.detail.value
    });
  },

  // 设置开始打卡时间
  setSignTime: function (e) {
    var that = this;
    var task = this.data.task;
    if (task.signEarlyTime >= e.detail.value) {
      this.handleWarning();
      return
    }
    that.setData({
      'task.signTime': e.detail.value
    });
  },
  // 设置截止打卡时间
  setSignEarlyTime: function (e) {
    var that = this;
    var task = this.data.task;
    if (task.signTime <= e.detail.value) {
      this.handleWarning();
      return
    }
    that.setData({
      'task.signEarlyTime': e.detail.value
    });
  },

  // 设置开始日期
  startDateChange: function (e) {
    this.setData({
      'task.startDay': e.detail.value
    })
  },

  // 设置结束日期
  endDateChange: function (e) {
    this.setData({
      'task.endDay': e.detail.value
    })

  },

  // 设置重复日
  changeMonday: function (e) {
    var state = this.data.task.monday;
    this.setData({
      'task.monday': (state == 1 ? 0 : 1)
    });
  },
  changeTuesday: function (e) {
    var state = this.data.task.tuesday;
    this.setData({
      'task.tuesday': (state == 1 ? 0 : 1)
    });
  },
  changeWednesday: function (e) {
    var state = this.data.task.wednesday;
    this.setData({
      'task.wednesday': (state == 1 ? 0 : 1)
    });
  },
  changeThursday: function (e) {
    var state = this.data.task.thursday;
    this.setData({
      'task.thursday': (state == 1 ? 0 : 1)
    });
  },
  changeFriday: function (e) {
    var state = this.data.task.friday;
    this.setData({
      'task.friday': (state == 1 ? 0 : 1)
    });
  },
  changeSaturday: function (e) {
    var state = this.data.task.saturday;
    this.setData({
      'task.saturday': (state == 1 ? 0 : 1)
    });
  },
  changeSunday: function (e) {
    var state = this.data.task.sunday;
    this.setData({
      'task.sunday': (state == 1 ? 0 : 1)
    });
  },

  // 隐藏提示弹层
  modalChange: function (e) {
    this.setData({
      modalHidden: true
    })
  },
    // 隐藏提示弹层
    modalChange: function (e) {
      this.setData({
        modalHidden: true
      })
    },
  
    // 打开选择图片
    changeImage() {
      this.setData({
        dialogShow: true
    })
    },
    // 选择图片
    radioChange(e) {
      console.log(e)
      this.setData({
        'task.imageId': this.data.imageList[e.detail.value]._id,
        imageName: this.data.imageList[e.detail.value].name
      })
    },
    // 确认图片
    tapDialogButton(e) {
      this.setData({
          dialogShow: false
      })
  },
    // 预览图片
    previewImage(e) {
     console.log(e)
     wx.previewImage({
      current: e.target.id,
      urls: [e.target.id]
     })
  },
  // 查询所有图片列表
  queryImageList(task) {
    let that = this;
    let db = wx.cloud.database()
    db.collection('clock_in_text').get({
      success: res => {
        var imageName = ''
        for (let i in res.data){
          if (task.imageId == res.data[i]._id)
            imageName = res.data[i].name
        }
        that.setData({
          imageList: res.data,
          imageName: imageName
        })
      }
    })
  },
  // 修改任务
  createTask: function () {
    var that = this;
    var task = this.data.task;
    const db = wx.cloud.database()
    wx.showToast({
      title: '修改中',
      icon: 'loading',
      duration: 5000
    });
    console.log(task)
    db.collection('clock_project').doc(task._id).update({
      data: {
        name: task.name, // 打卡名称
        signEarlyTime: task.signEarlyTime, // 开始时间
        signTime: task.signTime, // 截止时间
        startDay: task.startDay, // 开始日期
        endDay: task.endDay, // 截止日期
        monday: task.monday,
        tuesday: task.tuesday,
        wednesday: task.wednesday,
        thursday: task.thursday,
        friday: task.friday,
        saturday: task.saturday,
        sunday: task.sunday,
        remark: task.remark,// 备注
        imageId: task.imageId // 图片信息ID
      },
      success: res => {
        // 在返回结果
        wx.showToast({
          title: '修改成功',
        })
        setTimeout(function () {
          wx.navigateBack({

          })
        }, 800)
        // console.log('[数据库] [新增记录] 成功，记录: ', res)
      },
      fail: err => {
        wx.showToast({
          icon: 'none',
          title: '修改失败'
        })
        console.error('[数据库] [新增记录] 失败：', err)
      }
    })

  },

  // 提交、检验
  bindSubmit: function (e) {
    var that = this;
    var task = this.data.task;
    var creating = this.data.creating;

    if (task.name == '') {
      this.setData({
        modalHidden: false
      });
    } else {
      if (!creating) {
        this.setData({
          'creating': true
        });
        that.createTask();
      }
    }
  },

  onShow: function () {
    // 恢复新建按钮状态
    this.setData({
      'creating': false
    });
  },

  onHide: function () {
  },

  // 初始化设置
  onLoad: function (option) {
    const eventChannel = this.getOpenerEventChannel()
    var that = this;
    // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
    eventChannel.on('acceptDataFromOpenerPage', function (data) {
      that.setData({
        task: data.data
      })
       // 查询图片列表
      that.queryImageList(data.data)
    })

  }
})
