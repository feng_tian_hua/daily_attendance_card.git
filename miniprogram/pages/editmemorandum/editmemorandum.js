const app = getApp();
var util = require('../../utils/util.js');
var _openid = '';
Page({
  data: {
    formats: {},
    readOnly: false,
    placeholder: '开始输入...',
    editorHeight: 300,
    keyboardHeight: 0,
    addTime: '',
    updateTime: '',
    time: '',
    _id: '',
    // title: '',
    timetext: '',
    noteTitle: '',
    qlHeight: 0,
    note: '',
    isIOS: false,
    showSave: false // 显示保存按钮
  },
  readOnlyChange() {
    this.setData({
      readOnly: !this.data.readOnly
    })
  },
  onLoad() {
    const eventChannel = this.getOpenerEventChannel()
    var that = this;
    // 监听acceptDataFromOpenerPage事件，获取上一页面通过eventChannel传送到当前页面的数据
    eventChannel.on('acceptDataFromOpenerPage', function (data) {
      that.formatDate(data.data.addTime)
      that.setData({
        note: data.data.doc.ops,
        noteTitle: data.data.noteTitle,
        _id: data.data._id
      })
    })
    setTimeout(
      function () {
        _openid = app.getOpenid();
      }, 1000)
    const platform = wx.getSystemInfoSync().platform
    const isIOS = platform === 'ios'
    this.setData({
      isIOS
    })
    this.updatePosition(0)
    let keyboardHeight = 0
    wx.onKeyboardHeightChange(res => {
      if (res.height === keyboardHeight) return
      const duration = res.height > 0 ? res.duration * 1000 : 0
      keyboardHeight = res.height
      setTimeout(() => {
        wx.pageScrollTo({
          scrollTop: 0,
          success() {
            that.updatePosition(keyboardHeight)
            that.editorCtx.scrollIntoView()
          }
        })
      }, duration)

    })
  },
  // 获取标题
  bindKeyInput(e) {
    this.chageShowSave()
    this.setData({
      noteTitle: e.detail.value
    })
  },
  updatePosition(keyboardHeight) {
    var that = this
    var query = wx.createSelectorQuery();
    query.select('.title').boundingClientRect();
    query.select('.time').boundingClientRect();
    query.selectViewport().scrollOffset();
    query.exec((res) => {
      var titleHeight = res[0].height; // 获取list高度
      var timeHeight = res[1].height; // 获取list高度
      const toolbarHeight = 50
      const {
        windowHeight,
        platform
      } = wx.getSystemInfoSync()
      let editorHeight = keyboardHeight > 0 ? (windowHeight - keyboardHeight - toolbarHeight - titleHeight - timeHeight) : windowHeight
      that.setData({
        editorHeight,
        keyboardHeight
      })
    });

  },
  calNavigationBarAndStatusBar() {
    const systemInfo = wx.getSystemInfoSync()
    const {
      statusBarHeight,
      platform
    } = systemInfo
    const isIOS = platform === 'ios'
    const navigationBarHeight = isIOS ? 44 : 48
    return statusBarHeight + navigationBarHeight
  },
  // 显示保存按钮
  chageShowSave() {
    this.setData({
      showSave: true
    })
  },
  onEditorReady: async function () {
    const that = this
    wx.createSelectorQuery().select('#editor').context(function (res) {
      that.editorCtx = res.context
      // that.getContent()
      // that.querynoteinfo()
      setTimeout(function () {
        that.editorCtx.setContents({
          delta: that.data.note,
          success: res => {
            console.log('富文本赋值成功', res)
          },
          fail: error => {
            console.log(error)
          }
        })
      }, 100)
    }).exec()

  },
  blur() {
    this.editorCtx.blur()
  },
  format(e) {
    let {
      name,
      value
    } = e.target.dataset
    if (!name) return
    // console.log('format', name, value)
    this.editorCtx.format(name, value)

  },
  onStatusChange(e) {
    const formats = e.detail
    this.setData({
      formats
    })
  },
  insertDivider() {
    this.editorCtx.insertDivider({
      success: function () {
        console.log('insert divider success')
      }
    })
  },
  clear() {
    this.editorCtx.clear({
      success: function (res) {
        console.log("clear success")
      }
    })
  },
  removeFormat() {
    this.editorCtx.removeFormat()
  },
  insertDate() {
    const date = new Date()
    const formatDate = date
    this.editorCtx.insertText({
      text: formatDate
    })
  },
  //   //初始化富文本编辑器

  //获取编辑器输入的内容
  getContent() {
    const that = this
    const db = wx.cloud.database()
    that.editorCtx.getContents({
      success: res => {
        // 更新
        db.collection('memorandum').doc(that.data._id).update({
          data: {
            doc: res.delta,
            noteTitle: that.data.noteTitle,
            addTime: new Date()
          },
          success: res => {
            this.setData({
              updateTime: new Date(),
              showSave: false
            })
            that.formatDate(that.data.updateTime)
          }
        })
      },
      fail: error => {
        console.log(error)
      },

    })
  },
  // 插入图片
  insertImage() {
    const that = this
    wx.chooseImage({
      count: 1,
      success: function (res) {
        console.log(res)
        wx.cloud.uploadFile({
          cloudPath: 'memorandum/uploadimages/' + _openid + '/' + Math.floor(Math.random() * 1000000) + res.tempFilePaths[0].match(/\.[^.]+?$/)[0],
          filePath: res.tempFilePaths[0],
          success(res) {
            console.log("成功", res);
            that.editorCtx.insertImage({
              src: res.fileID,
              data: {
                id: 'abcd',
                role: 'god'
              },
              width: '80%',
              success: function () {
                console.log('insert image success')
              }
            })
          }
        })

      }
    })
  },
  inittime: function () {
    this.setData({
      addTime: new Date(),
      time: util.getHM(new Date())
    })
  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() { //返回显示页面状态函数
    //错误处理
    this.inittime(); //再次加载，实现返回上一页页面刷新
    //正确方法
    //只执行获取地址的方法，来进行局部刷新
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady(e) {

  },
  // 时间初始化
  formatDate(time) {
    var nowtime = new Date()
    nowtime.setHours(0);
    nowtime.setMinutes(0);
    nowtime.setSeconds(0);
    nowtime.setMilliseconds(0);
    var resulttime = (util.getTimestamp(time) - util.getTimestamp(nowtime)) / 3600

    if (resulttime > 0 && resulttime <= 24) {
      this.setData({
        timetext: '今天',
        updateTime: util.getHM(time)
      })
    } else if (resulttime < 0 && resulttime >= -24) {
      this.setData({
        timetext: '昨天',
        updateTime: util.getHM(time)
      })
    } else {
      this.setData({
        timetext: util.getMD(time),
        updateTime: util.getHM(time)
      })
    }

  }
})
