// pages/find/find.js
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		notice: ''
	},
	//跳转排行至页面
	onRanking: function () {
		wx.navigateTo({
			url: '../ranking/ranking',
		})
	},
	//跳转到位置页面
	onPosition: function () {
		wx.navigateTo({
			url: '../position/position',
		})
	},
	onSchool: function () {
		wx.navigateTo({
			url: '../campusMap/campusMap',
		})
	},
	onClockInNum:function(){
		wx.navigateTo({
			url: '../clockInNum/clockInNum',
		})
	},
	// 跳转至备忘录
	onmemorandum:function(){
		wx.navigateTo({
			url: '../memorandum/memorandum',
		})
	},
	//跳转至二维码生成器
	onQRcode:function(){
		wx.navigateTo({
			url: '../qrcode/qrcode',
		})
	},
	//跳转至番茄时钟
	onTClock:function(){
		wx.navigateTo({
			url: '../tomato_clock/t_clock',
		})
	},
	//跳转至我的足迹
	onMyFoot:function(){
		wx.navigateTo({
			url: '../myFoot/MyFoot',
		})
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		let that = this
			const db = wx.cloud.database();
			db.collection('notice').get(
				{
					success: res => {
						console.log(res)
             that.setData({
							 notice: res.data[0].content
						 })
					}
				}
			)
	},
  room: function(){
    wx.navigateTo({
      url: '../im/room/room',
    })
  },
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})