// pages/focusNewsDetail/focusNewsDetail.js
var descid=getApp().requestId;
Page({

  /**
   * 页面的初始数据
   */
  data: {
      id: 0,
      desc:[]
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    
    this.setData({
      id: options.Id,
      desc:['明理楼由北楼，中楼和南楼组成，按地理方向分布，南楼和北楼的教室较大，平时都是用于整个专业上课，中楼的教室相对较小，一般只能容纳一个班级上课，平时上课的话如果是理论课之类的都会在这边进行','cloud://hhufu-yaw47.6868-hhufu-yaw47-1301492670/images/体育馆.jpg']
    })
  },

  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {

  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {

  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
})