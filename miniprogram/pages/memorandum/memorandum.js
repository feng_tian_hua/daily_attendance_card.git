const app = getApp();
var util = require('../../utils/util.js');
const db = wx.cloud.database();
var _openid = '';
Page({
  data: {
    inputShowed: false,
    inputVal: "",
    buttonShow: true, // 添加按钮显示
    ifBind: false, // 复选框选中标识
    _id: '',
    index: 0,
    choiceShow: false, // 复选框显示
    tabbarShow: false, // 长按显示选项
    showLeft: '', // 长按左浮动控制
    nodetail: true,
    checkAll: false,
    time: '',
    memorandum: '',
    checkboxItems: [], // 复选框存储id
    serchinput: '' // 搜索框输入内容
  },
  // 查询备忘录便签
  listMemorandum: function () {
    // db.collection('memorandum').where({
    //   _openid: _openid
    // }).get({
    //   success: res => {
    //     this.init(res.data)
    //   },
    //   fail: err => {
    //     wx.showToast({
    //       icon: 'none',
    //       title: '查询失败',
    //     })
    //     console.log(' 获取 clock_in 失败，请检查是否有部署云函数，错误信息：', err)
    //   }
    // });
    this.search()
  },
  // 初始化、数据处理
  init(data){
    if (data.length != 0) {
      var info = [{}]
      console.log(data)
      for (var i = 0; i < data.length; i++) {
        var content = data[i].doc.ops[0].insert
        if (typeof content == 'object')
            content = '图片'
        var _date = new Date(data[i].addTime);
        var date_value = util.getMD(_date);
        info[i] = {
          _id: data[i]._id,
          addTime: date_value,
          noteContent: content,
          noteTitle: data[i].noteTitle
        }
      }
      this.setData({
        memorandum: info
      });
    } else {
      this.setData({
        memorandum: ''
      });
    }
  },
  // 复选框
  checkboxChange: function (e) {
    console.log('checkbox发生change事件，携带value值为：', e.detail.value);
    this.setData({
      checkboxItems: e.detail.value
    })
  },
  // 
  tabbarChange(e){
    console.log('tab change', e)
  },
  onLoad() {
    this.setData({
      search: this.search.bind(this)
    })
    wx.startPullDownRefresh() //下拉刷新
  },
  // 搜索
  search: function (value) {
    console.log('调用 ')
    let key = this.data.serchinput
    const db = wx.cloud.database();
    const _ = db.command
    db.collection('memorandum').where(_.or([{
      noteTitle: db.RegExp({
        regexp: '.*' + key,
        options: 'i',
      })
    },
    {
      'doc.ops.insert': db.RegExp({
        regexp: '.*' + key,
        options: 'i',
      })
    }
  ])).get({
    success: res => {
      console.log(res)
      this.init(res.data)
    },
    fail: err => {
      console.log(err)
    }
  })

  },
  bindclear(){
    this.listMemorandum()
  },
  // 获取搜索输入内容
  serchinput(e){
    console.log(e.detail.value)
    this.setData({
      serchinput: e.detail.value
    })
  },
  // 搜索结果
  selectResult: function (e) {
    console.log('select result', e.detail)
  },
  // 左滑按钮触发
  slideButtonTap(e) {
    console.log('slide button tap', e.detail)
    if (e.detail.index == 0) {
      // this.notedetail(e.detail);
    } else {
      this.deletenote(e.detail);
    }
  },
  cancel(){
    this.setData({
      choiceShow: false,
      showLeft: '',
      buttonShow: true,
      tabbarShow: false,
      nodetail: true
    })
  },
  // 长按触发事件
  longPress: function () {
    this.setData({
      choiceShow: true,
      showLeft: 'noteleft',
      buttonShow: false,
      tabbarShow: true,
      nodetail: false
    })
  },
  // 添加
  addnote: function () {
    wx.navigateTo({
      url: '../addmemorandum/addmemorandum',
    })
  },
  // 批量删除
  deletelist(idList){
    const that = this
    if(this.data.checkboxItems.length > 0){
    wx.showModal({
      title: '提示',
      content: '确认删除这'+ this.data.checkboxItems.length +'条便签吗？',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定');
          for(var i = 0; i < that.data.checkboxItems.length; i++){
          if (that.data.checkboxItems[i]) {
            const db = wx.cloud.database()
            db.collection('memorandum').doc(that.data.checkboxItems[i]).remove({
              success: res => {
                wx.showToast({
                  title: '删除成功',
                })
                setTimeout(function () {
                  that.cancel()
                  that.listMemorandum();
                }, 600)
              },
              fail: err => {
                wx.showToast({
                  icon: 'none',
                  title: '删除失败',
                })
                console.error('[数据库] [删除记录] 失败：', err)
              }
            })
          } else {
            wx.showToast({
              title: '无记录可删，请见创建一个记录',
            })
          }
        }
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  }else {
    wx.showToast({
      title: '请选择便签！',
      icon: 'error',
    })
  }
  },
  //全选与反全选
  checkAll: function (e) {
    var that = this;
    if(that.data.checkAll){
      this.setData({
        checkAll: false
      })
    } else {
      this.setData({
        checkAll: true
      })
    }
    if(that.data.checkAll){
      var arr = [];   //存放选中id的数组
      for (var i = 0; i < that.data.memorandum.length; i++) {
          // 全选获取选中的值
          arr = arr.concat(that.data.memorandum[i]._id.split(','));
      }
    } else {
      // 反全选
      arr = []
    } 
    that.setData({
      checkboxItems: arr
    })
    console.log(that.data.checkboxItems)
  },

  // 删除
  deletenote: function (e) {
    console.log(e)
    var _id = e.data._id;
    var that = this;
    wx.showModal({
      title: '提示',
      content: '确认删除该便签吗？',
      success(res) {
        if (res.confirm) {
          console.log('用户点击确定');
          if (_id) {
            const db = wx.cloud.database()
            db.collection('memorandum').doc(_id).remove({
              success: res => {
                wx.showToast({
                  title: '删除成功',
                })
                setTimeout(function () {
                  that.listMemorandum();
                }, 600)
              },
              fail: err => {
                wx.showToast({
                  icon: 'none',
                  title: '删除失败',
                })
                console.error('[数据库] [删除记录] 失败：', err)
              }
            })
          } else {
            wx.showToast({
              title: '无记录可删，请见创建一个记录',
            })
          }
        } else if (res.cancel) {
          console.log('用户点击取消')
        }
      }
    })
  },
  // 详情，修改
  notedetail: function (e) {
    var _id = e.currentTarget.id;
    var noteinfo = {}
    const that = this
    const db = wx.cloud.database();
    db.collection('memorandum').doc(_id).get({
      success: function (res) {
        // res.data 包含该记录的数据
        noteinfo = res.data;
        if(that.data.nodetail){
          setTimeout(function () {
            wx.navigateTo({
              url: '../editmemorandum/editmemorandum',
              success: function (res) {
                // 通过eventChannel向被打开页面传送数据
                res.eventChannel.emit('acceptDataFromOpenerPage', {
                  data: noteinfo
                })
              }
            })
          }, 300)
        } else {
          wx.showToast({
            title: '请点击右侧方框进行选择',
            icon: 'error',
          })
        }
      }
    })
  
  },
  /**
   * 生命周期函数--监听页面初次渲染完成
   */
  onReady: function () {

  },

  /**
   * 生命周期函数--监听页面显示
   */
  onShow() { //返回显示页面状态函数
    var that = this;
    setTimeout(function () {
      _openid = app.getOpenid()
      that.listMemorandum()
    }, 1000)
  },

  /**
   * 生命周期函数--监听页面隐藏
   */
  onHide: function () {

  },

  /**
   * 生命周期函数--监听页面卸载
   */
  onUnload: function () {
    console.log('返回')
    this.setData({
      choiceShow: false,
      showLeft: '',
      buttonShow: true,
      tabbarShow: false
    })

  },

  /**
   * 页面相关事件处理函数--监听用户下拉动作
   */
  onPullDownRefresh: function () {
    var that = this
    wx.showNavigationBarLoading() //在标题栏中显示加载
    setTimeout(function () {
      that.listMemorandum()
      wx.hideNavigationBarLoading() //完成停止加载
      wx.stopPullDownRefresh() //停止下拉刷新
    }, 1000);
  },

  /**
   * 页面上拉触底事件的处理函数
   */
  onReachBottom: function () {

  },

  /**
   * 用户点击右上角分享
   */
  onShareAppMessage: function () {

  }
});
