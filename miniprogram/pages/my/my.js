// pages/my/my.js
const app = getApp()
Page({

	/**
	 * 页面的初始数据
	 */
  data: {
    avatarUrl: './user-unlogin.png',
    userInfo: {},
    logged: false,
    takeSession: false,
    userid: '',
    nickName: '',
    //管理员openid
    vip:''
  },
  onLoad: function () {
    var that = this
    that.data.userid = app.getOpenid();
    const db = wx.cloud.database();
    db.collection('authority').where({
      identification: that.data.userid
    }).get({
      success: function(res){
        if(res.data.length>0){
          that.setData({
          vip: that.data.userid
        })
      }
      },
    })
    this.setData({
      avatarUrl: userInfo.avatarUrl,
      nickName: userInfo.nickName,
    })
  },
  onshow: function () {

  },
  openSetting: function () {
    wx.openSetting({
      success(res) {
        console.log(res.authSetting)
        // res.authSetting = {
        //   "scope.userInfo": true,
        //   "scope.userLocation": true
        // }
      }
    })
  },
  getUserInfo: function () {
    let that = this
    // 获取用户信息
    wx.getUserProfile({
      desc: '用于展示个人资料', // 声明获取用户个人信息后的用途，后续会展示在弹窗中，请谨慎填写
      success: function(res) {
        that.setData({
          avatarUrl: res.userInfo.avatarUrl,
          nickName: res.userInfo.nickName
        })
      }
    })
    
  }

})
