// pages/myClockIn/myClockIn.js
const app = getApp();
var util = require('../../utils/util.js');
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		clock_in: [{}],
		slideButtons: [{}],
		current: 1,
		pageSize: 0,
		show: true
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		// 调用函数
		this.setData({
			slideButtons: [{
				text: '删除',
				type: 'warn',
				src: '', // icon的路径
			}],
		});
    this.queryData(0);
	},
	// 查询数据
  queryData(pageNum) {
		this.setData({
			show: true
		})
		var userid = app.getOpenid();
    var that = this;
		wx.cloud.callFunction({
			name: 'callback',
			data: {_openid: userid, pageNum: pageNum},
			success: res => {

				this.setData({
					show: false
				})
				if (res.result.total % 10 > 0) {
					that.setData({
						pageSize: parseInt(res.result.total/10) == 0? 1: parseInt(res.result.total/10) + 1
					})
				} else {
					that.setData({
						pageSize: parseInt(res.result.total/10) == 0? 1: parseInt(res.result.total/10)
					})
				}
				
				var playurl = res.result.data;
        var timefo = [{}];
        for (var i = 0; i < playurl.length;i++){
          var date = new Date(playurl[i].time);
        var date_value = util.formatTime(date);
          timefo[i] = { time: date_value, name: playurl[i].name, _id: playurl[i]._id};
        }
        this.setData({
          clock_in: timefo
        })
			},
			fail: err => {
				console.error('[云函数] [callback] 调用失败',err)
				wx.navigateTo({
					url: '../deployFunctions/deployFunctions',
				})
			}
		})
	},

  slideButtonTap(e) {
		console.log('slide button tap', e.detail)
},
// 分页显示
handleChange (e) {
	console.log(e)
	const type = e.currentTarget.id;
	if (type === 'next') {
			this.setData({
					current: this.data.current + 1
			});
	} else if (type === 'prev') {
			this.setData({
					current: this.data.current - 1
			});
	}

	this.queryData(this.data.current -1);
},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})