// pages/myClockInPlay/myClockInPlay.js
const app = getApp();
var userid = '';
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		clockInPlay: '',
		_id: '',
	},
	slideButtonTap(e) {
		if(e.detail.index == 0){
			this.edit(e.detail.data._id)
		} else {
			this.deClockInPlay(e.detail.data._id);
		}
	},
	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		// 调用函数
		userid = app.getOpenid();
	},
	listClockInPlay: function () {
		const db = wx.cloud.database();
		db.collection('clock_project').where({
			_openid: userid
		}).skip(0)
			.get({
				success: res => {
					this.setData({
						clockInPlay: res.data
					})
					// console.log('[数据库] [查询记录] 成功: ', this.data.clockInPlay)
				},
				fail: err => {
					wx.showToast({
						icon: 'none',
						title: '查询失败',
					})
					console.log(' 获取 clock_in 失败，请检查是否有部署云函数，错误信息：', err)
				}
			});
	},
	// 添加打卡记录
	addClockIn: function () {

		wx.navigateTo({    //保留当前页面，跳转到应用内的某个页面（最多打开5个页面，之后按钮就没有响应的）
			url: "../new/new"

		})
	},
	// slideButtonTap(e) {
	// 	console.log('slide button tap', e.detail)
	// },
	deClockInPlay: function (e) {
		var _id = e;
		var userid = app.getOpenid();
		var that = this;
		wx.showModal({
			title: '提示',
			content: '确认删除该打卡计划吗？',
			success(res) {
				if (res.confirm) {
					console.log('用户点击确定');
					if (_id) {
						const db = wx.cloud.database()
						db.collection('clock_project').doc(_id).remove({
							success: res => {
								wx.showToast({
									title: '删除成功',
								})
								setTimeout(function () {
									that.listClockInPlay();
								}, 600)
							},
							fail: err => {
								wx.showToast({
									icon: 'none',
									title: '删除失败',
								})
								console.error('[数据库] [删除记录] 失败：', err)
							}
						})
					} else {
						wx.showToast({
							title: '无记录可删，请见创建一个记录',
						})
					}
				} else if (res.cancel) {
					console.log('用户点击取消')
				}
			}
		})

	},
	edit: function (e) {
		var _id = e;
		const db = wx.cloud.database();
		var info = {}
		db.collection('clock_project').doc(_id).get({
			success: function (res) {
				// res.data 包含该记录的数据
				info = res.data;
			}
		})
		setTimeout(function () {
			wx.navigateTo({
				url: '../editClockIn/editClockIn?id=' + _id,
				success: function (res) {
					// 通过eventChannel向被打开页面传送数据
					res.eventChannel.emit('acceptDataFromOpenerPage', { data: info })
				}
			})
		}, 800)
	},
	onDisplay: function (e) {
		var _id = e.currentTarget.id;
		if (this.data._id == _id) {
			_id = 0;
		}
		this.setData({
			_id: _id
		})
		// console.log(_id);
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow() { //返回显示页面状态函数
		//错误处理
		this.listClockInPlay();//再次加载，实现返回上一页页面刷新
		//正确方法
		//只执行获取地址的方法，来进行局部刷新
	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})