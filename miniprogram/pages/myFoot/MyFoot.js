// miniprogram/pages/myFoot/MyFoot.js

const app = getApp();
Page({

	/**
	 * 页面的初始数据
	 */
	data: {
		address: '',
		latitude: '',
		longitude: '',
		detail: '',
		markers: [],
		times: '0',
		citys: [],
		_openid: '',
		clock_in: '',
		showCon: false,
		showCard: false,
	},

	/**
	 * 生命周期函数--监听页面加载
	 */
	onLoad: function (options) {
		this.getCityNameOFLocation();
		this.setData({
			_openid: app.getOpenid()
		})
		var that = this;
		setTimeout(function () {
			that.onList();
			that.onCard();
		}, 1000)
	},
	getCityNameOFLocation: function () {
		var that = this;
		wx.getLocation({
			type: 'gcj02', // 默认为 wgs84 返回 gps 坐标，gcj02 返回可用于 wx.openLocation 的坐标
			success: function (res) {
				console.log("定位成功");
				var latitude = res.latitude;
				var longitude = res.longitude;
				var locationString = latitude + "," + longitude;
				wx.request({
					url: 'https://apis.map.qq.com/ws/geocoder/v1/?l&get_poi=1',
					data: {
						"key": "L6PBZ-DDR3W-2WURZ-RRK44-XE7T6-74BDV",
						"location": locationString
					},
					method: 'GET',
					// header: {},
					success: function (res) {
						// success
						console.log(res.data);
						var address = res.data.result.address
						var detail = res.data.result.formatted_addresses.rough
						console.log("请求数据:" + address);
						that.setData({
							address: address,
							detail: detail,
							latitude: latitude,
							longitude: longitude
						})
					},
					fail: function () {
						// fail
						console.log("请求失败");
					},
					complete: function () {
						// complete
						console.log("请求完成");
					}
				})
			},
			fail: function () {
				// fail
				console.log("定位失败");
				wx.getSetting({
					success: (res) => {
						if (!res.authSetting['scope.userLocation']) {
							that.setData({
								showCon: true
							})
						}
					}
				})
			},
			complete: function () {
				// complete
				console.log("定位完成");
			}
		})
	},
	//查询打卡记录
	onList: function () {
		// const db = wx.cloud.database();
		// const $ = db.command.aggregate;
		var that = this;
		// db.collection('position').where({
		// 	 _openid: that.data._openid
		//  }).skip(0).orderBy('time', 'desc')
		// 	 .get({
		// 		 success: res => {
		// 			 console.log('=>onList',res.data);
		// 			 if(res.data.length>0){
		// 			 var marker = []
		// 				for (var i = 0; i < res.data.length; i++) {

		// 					var info = {
		// 						iconPath: "",
		// 						id: 0,
		// 						latitude: '',
		// 						longitude: '',
		// 						width: 35,
		// 						height: 35,
		// 						title:""
		// 					};
		// 					info.id = i
		// 					info.latitude = res.data[i].latitude
		// 					info.longitude = res.data[i].longitude
		// 					info.title = res.data[i].address
		// 					marker.push(info)
		// 				}

		// 			 this.setData({
		// 				 markers: marker,
		// 				 times:total
		// 			 })
		// 			 }
		// 			 // console.log('[数据库] [查询记录] 成功: ', this.data.clock_in)
		// 		 },
		// 		 fail: err => {
		// 			 wx.showToast({
		// 				 icon: 'none',
		// 				 title: '查询失败',
		// 			 })
		// 			 console.log(' 获取 clock_in 失败，请检查是否有部署云函数，错误信息：', err)
		// 		 }
		// 	 });
		wx.cloud.callFunction({
			// 自己定义的云函数名称
			name: 'getmyfoot',
			// 传给云函数的参数
			data: {
				openid: that.data.openid
			},
			success: function (res) {
				//这里的res就是云函数的返回值
				console.log(res)
				if (res.result.total > 0) {
					var marker = []
					var list= res.result.list
					for (var i = 0; i < list.length; i++) {
						var info = {
							iconPath: "",
							id: 0,
							latitude: '',
							longitude: '',
							width: 35,
							height: 35,
							title: ""
						};
						info.id = i
						info.latitude = list[i].latitude
						info.longitude = list[i].longitude
						info.title = list[i].address
						marker.push(info)
					}
					that.setData({
						markers: marker,
						times: res.result.total
					})
				}
			},
			fail: console.error
		})

	},
	onCard: function () {
		const db = wx.cloud.database();
		const $ = db.command.aggregate;
		db.collection('position').aggregate().group({
			_id: '$city',
			num: $.sum(1)
		}).end({
			success: res => {
				console.log("=>citySuccess", res.list);
				this.setData({
					citys: res.list
				})
			},
			fail: err => {
				console.log("=>cityErr", err);
			}
		});
	},
	//控制展示数据面板
	onShowCard: function () {
		var flag = true;
		if (this.data.showCard == flag) {
			this.setData({
				showCard: false,
			})
		} else {
			this.setData({
				showCard: flag,
			})
		}
	},
	/**
	 * 生命周期函数--监听页面初次渲染完成
	 */
	onReady: function () {

	},

	/**
	 * 生命周期函数--监听页面显示
	 */
	onShow: function () {

	},

	/**
	 * 生命周期函数--监听页面隐藏
	 */
	onHide: function () {

	},

	/**
	 * 生命周期函数--监听页面卸载
	 */
	onUnload: function () {

	},

	/**
	 * 页面相关事件处理函数--监听用户下拉动作
	 */
	onPullDownRefresh: function () {

	},

	/**
	 * 页面上拉触底事件的处理函数
	 */
	onReachBottom: function () {

	},

	/**
	 * 用户点击右上角分享
	 */
	onShareAppMessage: function () {

	}
})