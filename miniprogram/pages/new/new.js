var util = require('../../utils/util.js');
var app = getApp();

Page({
  data: {
    task: {
      name: '', // 打卡名称
      signEarlyTime: '00:00', // 开始时间
      signTime: '00:00', // 截止时间
      startDay: '2016-11-00', // 开始日期
      endDay: '2016-11-00', // 截止日期
      monday: 1,
      tuesday: 1,
      wednesday: 1,
      thursday: 1,
      friday: 1,
      saturday: 0,
      sunday: 0,
      remark: '',// 备注
      imageId: '', // 图片信息ID
      num: 0
    },
    imageList: [], // 图片列表
    dialogShow: false,
    buttons: [{ text: '取消' }, { text: '确定' }],
    imageName: '图片1',
    creating: false,
    button: {
      txt: '新建'
    },
    modalHidden: true
  },
  // 提示
  handleWarning() {
    wx.showModal({
      title: '温馨提示',
      content: '开始时间不能大于截止时间',
      showCancel: false
    })
  },
  // 设置任务名称
  bindKeyInput: function (e) {
    this.setData({
      'task.name': e.detail.value
    });
  },
  // 设置备注
  bindContextInput: function (e) {
    this.setData({
      'task.remark': e.detail.value
    });
  },

  // 设置开始打卡时间
  setSignTime: function (e) {
    var that = this;
    var task = this.data.task;
    if (task.signEarlyTime >= e.detail.value) {
      this.handleWarning();
      return
    }
    that.setData({
      'task.signTime': e.detail.value
    });
  },
  // 设置截止打卡时间
  setSignEarlyTime: function (e) {
    var that = this;
    var task = this.data.task;
    if (task.signTime <= e.detail.value) {
      this.handleWarning();
      return
    }
    that.setData({
      'task.signEarlyTime': e.detail.value
    });
  },

  // 设置开始日期
  startDateChange: function (e) {
    this.setData({
      'task.startDay': e.detail.value
    })
  },

  // 设置结束日期
  endDateChange: function (e) {
    this.setData({
      'task.endDay': e.detail.value
    })

  },

  // 设置重复日
  changeMonday: function (e) {
    var state = this.data.task.monday;
    this.setData({
      'task.monday': (state == 1 ? 0 : 1)
    });
  },
  changeTuesday: function (e) {
    var state = this.data.task.tuesday;
    this.setData({
      'task.tuesday': (state == 1 ? 0 : 1)
    });
  },
  changeWednesday: function (e) {
    var state = this.data.task.wednesday;
    this.setData({
      'task.wednesday': (state == 1 ? 0 : 1)
    });
  },
  changeThursday: function (e) {
    var state = this.data.task.thursday;
    this.setData({
      'task.thursday': (state == 1 ? 0 : 1)
    });
  },
  changeFriday: function (e) {
    var state = this.data.task.friday;
    this.setData({
      'task.friday': (state == 1 ? 0 : 1)
    });
  },
  changeSaturday: function (e) {
    var state = this.data.task.saturday;
    this.setData({
      'task.saturday': (state == 1 ? 0 : 1)
    });
  },
  changeSunday: function (e) {
    var state = this.data.task.sunday;
    this.setData({
      'task.sunday': (state == 1 ? 0 : 1)
    });
  },

  // 隐藏提示弹层
  modalChange: function (e) {
    this.setData({
      modalHidden: true
    })
  },

  // 创建任务
  createTask: function () {
    var that = this;
    var task = this.data.task;

    wx.showToast({
      title: '新建中',
      icon: 'loading',
      duration: 5000
    });
    const db = wx.cloud.database();
    db.collection('clock_project').add({
      data: task,
      success: res => {
        // 在返回结果
        wx.showToast({
          title: '添加成功',
        })
        setTimeout(function () {
          wx.navigateBack({

          })
        }, 800)
        // console.log('[数据库] [新增记录] 成功，记录: ', res)
      },
      fail: err => {

      }
    })

  },

  // 提交、检验
  bindSubmit: function (e) {
    var that = this;
    var task = this.data.task;
    var creating = this.data.creating;

    if (task.name == '') {
      this.setData({
        modalHidden: false
      });
    } else {
      if (!creating) {
        this.setData({
          'creating': true
        });
        that.createTask();
      }
    }
  },
  // 打开选择图片
  changeImage() {
    this.setData({
      dialogShow: true
  })
  },
  // 选择图片
  radioChange(e) {
    console.log(e)
    this.setData({
      'task.imageId': this.data.imageList[e.detail.value]._id,
      imageName: this.data.imageList[e.detail.value].name
    })
  },
  // 确认图片
  tapDialogButton(e) {
    this.setData({
        dialogShow: false
    })
},
  // 预览图片
  previewImage(e) {
   console.log(e)
   wx.previewImage({
    current: e.target.id,
    urls: [e.target.id]
   })
},
  onShow: function () {
    // 恢复新建按钮状态
    this.setData({
      'creating': false
    });
  },

  onHide: function () {
  },

  // 初始化设置
  onLoad: function () {
    var that = this;
    var now = new Date();
    var openId = wx.getStorageSync('openId');

    // 查询图片列表
    this.queryImageList(that)
    // 初始化打卡时间
    that.setData({
      'task.signTime': util.getHM(now),
      'task.signEarlyTime': util.getHM(new Date(now.getTime() - 1000 * 3600 * 2))
    });

    // 初始化日期
    that.setData({
      'task.startDay': util.getYMD(now),
      'task.endDay': util.getYMD(now)
    });


    // 初始化昵称
    app.getUserInfo(function (userInfo) {
      //更新数据
      that.setData({
        userInfo: userInfo
      });

      that.setData({
        openId: openId
      })
    });

  },
  // 查询所有图片列表
  queryImageList(that) {
    let db = wx.cloud.database()
    db.collection('clock_in_text').get({
      success: res=> {
        console.log(res)
        that.setData({
          imageList: res.data,
          'task.imageId': res.data[0]._id
        })
      }
    })
  }
})
