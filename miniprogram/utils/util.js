//得到时间格式2018-10-02 10:00:50
const formatTime = date => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	const hour = date.getHours()
	const minute = date.getMinutes()
	const second = date.getSeconds()

	return [year, month, day].map(formatNumber).join('-') + ' ' + [hour, minute, second].map(formatNumber).join(':')
}

//格式化日期2018-10-02
const formatDate = date => {
	const year = date.getFullYear()
	const month = date.getMonth() + 1
	const day = date.getDate()
	return [year, month, day].map(formatNumber).join('-')
}
// 格式化时间为 04月08日 
function getMD (date) { 
  var month = date.getMonth() + 1 
  var day = date.getDate() 
 
  return [month, day].map(formatNumber).join('月') + '日' 
} 
//格式化数字
const formatNumber = n => {
	n = n.toString()
	return n[1] ? n : '0' + n
}
//todate默认参数是当前日期，可以传入对应时间 todate格式为2018-10-05
function getDates(days, todate) {
	var dateArry = [];
	for (var i = 0; i < days; i++) {
		var dateObj = dateLater(todate, i);
		dateArry.push(dateObj)
	}
	return dateArry;
}
function dateLater(dates, later) {
	let dateObj = {};
	let show_day = new Array('周日', '周一', '周二', '周三', '周四', '周五', '周六');
	let date = new Date(dates);
	date.setDate(date.getDate() + later);
	let day = date.getDay();
	let yearDate = date.getFullYear();
	let month = ((date.getMonth() + 1) < 10 ? ("0" + (date.getMonth() + 1)) : date.getMonth() + 1);
	let dayFormate = (date.getDate() < 10 ? ("0" + date.getDate()) : date.getDate());
	dateObj.time = yearDate + '-' + month + '-' + dayFormate;
	dateObj.week = show_day[day];
	return dateObj;
}
function constructor1() {
	this.now = new Date();
	this.nowYear = this.now.getYear(); //当前年 
	this.nowMonth = this.now.getMonth(); //当前月 
	this.nowDay = this.now.getDate(); //当前日 
	this.nowDayOfWeek = this.now.getDay(); //今天是本周的第几天 
	this.nowYear += (this.nowYear < 2000) ? 1900 : 0;
}
//获取某月的天数
function getMonthDays(myMonth) {
	let monthStartDate = new Date(this.nowYear, myMonth, 1);
	let monthEndDate = new Date(this.nowYear, myMonth + 1, 1);
	let days = (monthEndDate - monthStartDate) / (1000 * 60 * 60 * 24);
	return days;
}
//获取本季度的开始月份
function getQuarterStartMonth() {
	let startMonth = 0;
	if (this.nowMonth < 3) {
		startMonth = 0;
	}
	if (2 < this.nowMonth && this.nowMonth < 6) {
		startMonth = 3;
	}
	if (5 < this.nowMonth && this.nowMonth < 9) {
		startMonth = 6;
	}
	if (this.nowMonth > 8) {
		startMonth = 9;
	}
	return startMonth;
}
//获取本周的开始日期
function getWeekStartDate() {
	return this.formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay - this.nowDayOfWeek + 1));
}
//获取本周的结束日期
function getWeekEndDate() {
	return this.formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay + (6 - this.nowDayOfWeek + 1)));
}
//获取今天的日期
function getNowDate() {
	return this.formatDate(new Date(this.nowYear, this.nowMonth, this.nowDay));
}
//获取两个日期的天数差
function getDateDiff(sDate1, sDate2) {    //sDate1和sDate2是2002-12-18格式  
	var aDate, oDate1, oDate2, iDays
	oDate1 = new Date(sDate1)    //转换为12-18-2002格式  
	oDate2 = new Date(sDate2)
	iDays = parseInt(Math.abs(oDate1 - oDate2) / (1000 * 3600 * 24))
	//把相差的毫秒数转换为天数  
	return iDays
}
function getYMD(date) {
	var year = date.getFullYear()
	var month = date.getMonth() + 1
	var day = date.getDate()

	return [year, month, day].map(formatNumber).join('-')
}
function getHM(date) {
	var hour = date.getHours()
	var minute = date.getMinutes()

	return [hour, minute].map(formatNumber).join(':')
}


/**
 * 时间字符串转时间戳
 */
function getTimestamp(dataString) {
	// 获取某个时间格式的时间戳  
	var timestamp2 = Date.parse(new Date(dataString));
	// 如果想获取的时间戳为13位那就不用除1000
	timestamp2 = timestamp2 / 1000;
	return timestamp2
}


function getW(date) {
	var d = date.getDay();
	var arr = ['日', '一', '二', '三', '四', '五', '六'];
	return d
}


// 番茄时钟格式化倒计时
function t_formatTime(time, format) {
  let temp = '0000000000' + time
  let len = format.length
  return temp.substr(-len)
}

module.exports = {
	formatNumber: formatNumber,
	constructor1: constructor1,
	formatTime: formatTime,
	formatDate: formatDate,
	getDates: getDates,
	getMonthDays: getMonthDays,
	getQuarterStartMonth: getQuarterStartMonth,
	getWeekStartDate: getWeekStartDate,
	getNowDate: getNowDate,
	getWeekEndDate: getWeekEndDate,
	getDateDiff: getDateDiff,
	getYMD: getYMD,
	getHM: getHM,
	getMD: getMD,
	getW: getW,
	getTimestamp: getTimestamp,
	t_formatTime:t_formatTime
}
